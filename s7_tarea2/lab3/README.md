# Goals
Nginx sees PHP

## Commands

```
# Spin up all services defined in docker-compose.yml:
$ docker-compose up -d

# Execute Composer Install if you need to install any dependency:
$ docker-compose run composer

# Observe the services output in the logs. (Exit with Ctrl+C):
$ docker-compose logs -f

# Obtain a list of running containers
$ docker-compose ps

# Obtain a list of running processes
$ docker-compose top

# Once you're finished, stop all running services:
$ docker-compose down
```

## Exercise

Add a Redis service
