# Goals
Show basic setup for PHP
- Composer execution

## Commands

```
# Spin up all services defined in docker-compose.yml:
$ docker-compose up -d

# If you need to run a one-time command:
$ docker-compose run <service> <command>

# Observe the services output in the logs. (Exit with Ctrl+C):
$ docker-compose logs -f

# Obtain a list of running containers
$ docker-compose ps

# Obtain a list of running processes
$ docker-compose top

# Once you're finished, stop all running services:
$ docker-compose down
```

## Exercise

Install phpunit with Composer. Execute it through docker-compose
