# Goals
Spin up some services with Docker Compose

Access them from the outside (Nginx)

Show basic setup for Nginx
- Config files, for instace: virtual hosts

## Commands

```
# Spin up all services defined in docker-compose.yml:
$ docker-compose up -d

# Observe the services output in the logs. (Exit with Ctrl+C):
$ docker-compose logs -f

# Obtain a list of running containers
$ docker-compose ps

# Obtain a list of running processes
$ docker-compose top

# Once you're finished, stop all running services:
$ docker-compose down
```

## Exercise

Use our own Dockerfile that configures the site

