```
# Force usage of one configuration file:
$ docker-compose -f docker-compose.yml up

# Check out the output!!

# Now let's modify the output with an environmental variable:
$ SAYWHAT=aloha! docker-compose -f docker-compose.yml up

# Check out the output - it's different because of the env var!!

# Default usage picks both docker-compose.yml and docker-compose.override.yml:
$ docker-compose up

# Check out the output - it's different again but this time because of docker-compose.override.yml
```
